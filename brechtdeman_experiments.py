import numpy as np
import soundfile as sf
from scipy.signal import lfilter
import copy


def read_file(filename):
    data, fs = sf.read(filename, always_2d=True)
    return data, fs


def linear_to_db(float_value):
    if float_value > 1e-16:
        db_value = 10 * np.log10(float_value)
    else:
        db_value = -160
    return db_value


def db_to_linear(db_value):
    float_value = pow(10, db_value / 10)
    return float_value


def calculate_loudness(signal, fs, channel_gain_coeffs=[1.0, 1.0, 1.0, 1.41, 1.41]):
    # filter
    if len(signal.shape) == 1:  # if shape (N,), then make (N,1)
        signal_filtered = copy.copy(signal.reshape((signal.shape[0], 1)))
    else:
        signal_filtered = copy.copy(signal)

    for i in range(signal_filtered.shape[1]):
        signal_filtered[:, i] = k_filter(signal_filtered[:, i], fs)

    # mean square
    block_sec = 0.400  # 400 ms gating block
    abs_thresh = -70.0  # absolute threshold: -70 LKFS
    overlap = .75  # relative overlap (0.0-1.0)
    step = 1 - overlap

    data_len_sec = signal_filtered.shape[0] / fs  # length of measurement interval in seconds
    j_range = np.arange(0, (data_len_sec - block_sec) / (block_sec * step))
    z = np.ndarray(shape=(signal_filtered.shape[1], len(j_range)))
    # write in explicit for-loops for readability and translatability
    for i in range(signal_filtered.shape[1]):  # for each channel i
        for j in j_range:  # for each window j
            lbound = int(np.round(fs * block_sec * j * step).astype(int))
            hbound = int(np.round(fs * block_sec * (j * step + 1)).astype(int))
            z[int(i), int(j)] = (1 / (block_sec * fs)) * np.sum(np.square(signal_filtered[lbound:hbound, i]))
            print(z[int(i), int(j)])

    channel_gain_coeffs_true = np.array(channel_gain_coeffs[:signal_filtered.shape[1]])  # discard weighting coefficients G_i unused channels
    n_channels = channel_gain_coeffs_true.shape[0]
    l = [-.691 + 10.0 * np.log10(np.sum([channel_gain_coeffs_true[i] * z[i, j.astype(int)] for i in range(n_channels)])) \
         for j in j_range]
    print(l)

    # throw out anything below absolute threshold:
    indices_gated = [idx for idx, el in enumerate(l) if el > abs_thresh]
    z_avg = [np.mean([z[i, j] for j in indices_gated]) for i in range(n_channels)]
    Gamma_r = -.691 + 10.0 * np.log10(np.sum([channel_gain_coeffs_true[i] * z_avg[i] for i in range(n_channels)])) - 10.0
    # throw out anything below relative threshold:
    indices_gated = [idx for idx, el in enumerate(l) if el > Gamma_r]
    z_avg = [np.mean([z[i, j] for j in indices_gated]) for i in range(n_channels)]
    loudness_result = -.691 + 10.0 * np.log10(np.sum([channel_gain_coeffs_true[i] * z_avg[i] for i in range(n_channels)]))

    return loudness_result


def k_filter(signal, fs):
    b0 = 1.53512485958697
    b1 = -2.69169618940638
    b2 = 1.19839281085285
    a0 = 1.0
    a1 = -1.69065929318241
    a2 = 0.73248077421585

    signal_filtered_1 = lfilter([b0, b1, b2], [a0, a1, a2], signal)

    a0 = 1.0
    a1 = -1.99004745483398
    a2 = 0.99007225036621
    b0 = 1.0
    b1 = -2.0
    b2 = 1.0

    signal_filtered_2 = lfilter([b0, b1, b2], [a0, a1, a2], signal_filtered_1)

    return signal_filtered_2


data, sr = read_file("test_01.wav")

data = np.transpose(data)[0]

print(calculate_loudness(data, sr))

# MIT License

# Copyright (c) 2018 Brecht De Man

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.