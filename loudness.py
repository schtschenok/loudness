import soundfile as sf
import numpy as np
import scipy.signal

input_filename = "test_01.wav"


# Returns samplerate as int
def get_samplerate(filename):
    return sf.info(filename).samplerate


# Returns data and samplerate
def read_file(filename):
    data, samplerate = sf.read(filename, always_2d=True)
    return np.transpose(data), samplerate


# TODO: Check how it works exactly
def write_file(filename, data, samplerate):
    sf.write(filename, data, samplerate, subtype="FLOAT")


# Returns value in dB
def linear_to_db(float_value, ten_or_twenty=20):
    if float_value > 1e-16:
        db_value = ten_or_twenty * np.log10(float_value)
    else:
        db_value = -160
    return db_value


# Returns float value
def db_to_linear(db_value, ten_or_twenty=20):
    float_value = pow(ten_or_twenty, db_value / 10)
    return float_value


# Yields blocks
def chunker(data_array, samplerate, blocksize_ms, overlap_percent, incomplete_block=False):
    blocksize = int(blocksize_ms / 1000 * samplerate)
    print(blocksize)
    step = int((100 - overlap_percent) / 100 * blocksize)
    print(step)
    for index in range(blocksize, len(data_array) + step, step):
        if len(data_array[index - blocksize:index]) == blocksize:
            yield data_array[index - blocksize:index]
        elif incomplete_block is True:
            yield data_array[index - blocksize:index]


# TODO: Needs to be rewritten
def k_filter_48000(signal):
    b0 = 1.53512485958697
    b1 = -2.69169618940638
    b2 = 1.19839281085285
    a0 = 1.0
    a1 = -1.69065929318241
    a2 = 0.73248077421585

    signal_filtered_1 = scipy.signal.lfilter([b0, b1, b2], [a0, a1, a2], signal)

    a0 = 1.0
    a1 = -1.99004745483398
    a2 = 0.99007225036621
    b0 = 1.0
    b1 = -2.0
    b2 = 1.0

    signal_filtered_2 = scipy.signal.lfilter([b0, b1, b2], [a0, a1, a2], signal_filtered_1)

    return signal_filtered_2


for each in chunker(k_filter_48000(np.transpose(sf.read(input_filename)[0])), 48000, 400, 75, incomplete_block=False):
    print(np.mean(np.square(each)))
