import numpy as np
import scipy.signal
import soundfile as sf
import copy

input_filename = "Mono_24LUFS.wav"


def read_file(filename):
    data, fs = sf.read(filename, always_2d=True)
    return data, fs


def write_file(filename, data, samplerate):
    sf.write(filename, data, samplerate, subtype="FLOAT")


def linear_to_db(float_value):
    if float_value > 1e-16:
        db_value = 10 * np.log10(float_value)
    else:
        db_value = -160
    return db_value


def db_to_linear(db_value):
    float_value = pow(10, db_value / 10)
    return float_value


def k_filter(signal):
    b0 = 1.53512485958697
    b1 = -2.69169618940638
    b2 = 1.19839281085285
    a0 = 1.0
    a1 = -1.69065929318241
    a2 = 0.73248077421585

    signal_filtered_1 = scipy.signal.lfilter([b0, b1, b2], [a0, a1, a2], signal)

    a0 = 1.0
    a1 = -1.99004745483398
    a2 = 0.99007225036621
    b0 = 1.0
    b1 = -2.0
    b2 = 1.0

    signal_filtered_2 = scipy.signal.lfilter([b0, b1, b2], [a0, a1, a2], signal_filtered_1)

    return signal_filtered_2


input_data, input_samplerate = read_file(input_filename)

input_data = np.transpose(input_data)

filtered_data = k_filter(input_data)[0]

blocksize_ms, overlap = 400, 0.75

blocksize_samples = int(blocksize_ms / 1000 * input_samplerate)

step_samples = int(blocksize_samples * (1 - overlap))

ungated_blocks = np.empty(0)

for sample_index in range(blocksize_samples, len(filtered_data), step_samples):
    block = filtered_data[sample_index - blocksize_samples:sample_index]
    block_mean_square = np.mean(np.square(block))
    ungated_blocks = np.append(ungated_blocks, block_mean_square)

absolute_gated_blocks = np.empty(0)

for ungated_block in ungated_blocks:
    if ungated_block > db_to_linear(-70.0):
        absolute_gated_blocks = np.append(absolute_gated_blocks, ungated_block)

absolute_gated_loudness = linear_to_db(np.mean(absolute_gated_blocks))

relative_gated_blocks = np.empty(0)

relative_gating_threshold = absolute_gated_loudness - 10.0

for absolute_gated_block in absolute_gated_blocks:
    if absolute_gated_block > relative_gating_threshold:
        relative_gated_blocks = np.append(relative_gated_blocks, absolute_gated_block)

relative_gated_loudness = -.691 + linear_to_db(np.mean(relative_gated_blocks))

print(absolute_gated_loudness)
print(relative_gated_loudness)